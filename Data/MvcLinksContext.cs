using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using shorturl_cs.Models;

    public class MvcLinksContext : DbContext
    {
        public MvcLinksContext (DbContextOptions<MvcLinksContext> options)
            : base(options)
        {
        }

        public DbSet<shorturl_cs.Models.Links> Links { get; set; } = default!;
    }
