using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<MvcLinksContext>(options =>
    options.UseSqlite(builder.Configuration.GetConnectionString("MvcLinksContext") ?? throw new InvalidOperationException("Connection string 'MvcLinksContext' not found.")));


// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

//app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "ShortURL",
    pattern: "{id}",
    defaults: new { controller = "ShortUrl", action = "Redirect" }
);

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=ShortUrl}/{action=Index}/{id?}");
// var test = UrlPrameter.Mandatory;

app.Run();
