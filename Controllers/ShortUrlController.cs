using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Encodings.Web;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

using shorturl_cs.Models;

namespace shorturl_cs.Controllers
{
    public class ShortUrlController : Controller
    {
        private readonly MvcLinksContext _context;

        public ShortUrlController(MvcLinksContext context)
        {
            _context = context;
        }

        /*public string Index(string? id)
        {
            return "This is my default action. Id = " + id;
        }*/

        // 
        // GET: /HelloWorld/Welcome/ 

        public string Welcome()
        {
            return "This is the Welcome action method...";
        }

        /*public RedirectResult Redirect()
        {

            return Redirect("https://arsrobotics.org");
        }*/
/*        public ActionResult Redirect(string? id)
        {
            if (id == null)
            {
                RedirectToAction("Index", "Home");
            }

            //var link = _context.Links
            //    .FirstOrDefault(l => l.Id == id);

            var links = from l in _context.Links select l;
            
            //if (link == null)
            //{
            //    return NotFound();
            //}

            if (!String.IsNullOrEmpty(id))
            {
                link
            }

            string url = link.Url.ToString();

            return Redirect(url);
        }*/
        
        /*public async Task<IActionResult> Redirect(string id)
        {
            if (id == null || _context.Links == null)
            {
                return NotFound();
            }

            var links = await _context.Links.FindAsync(id);
            if (links == null)
            {
                return NotFound();
            }
            //return View(links);
            return await Redirect(links.Url);
        }*/

        public IActionResult Redirect(string id)
        {
            int step = 0;
            Console.WriteLine("Step {0}: ID = {1}", step, id);
            step++;
            if (id == null || _context.Links == null)
            {
                return NotFound();
            }

            Console.WriteLine("Step {0}: ID = {1}", step, id);
            step++;
            var links = _context.Links.Find(id);

            Console.WriteLine("Step {0}: ID = {1}", step, id);
            step++;
            if (links == null)
            {
                return NotFound();
            }

            Console.WriteLine("Step {0}: ID = {1}", step, id);
            step++;
            //return View(links);
            return Redirect(links.Url);
        }

        public async Task<IActionResult> Index()
        {
              return _context.Links != null ? 
                          View(await _context.Links.ToListAsync()) :
                          Problem("Entity set 'MvcLinksContext.Links'  is null.");
        }

        // GET: ShortUrlControllerSample/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null || _context.Links == null)
            {
                return NotFound();
            }

            var links = await _context.Links
                .FirstOrDefaultAsync(m => m.Id == id);
            if (links == null)
            {
                return NotFound();
            }

            return View(links);
        }

        // GET: ShortUrlControllerSample/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ShortUrlControllerSample/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Url")] Links links)
        {
            if (ModelState.IsValid)
            {
                _context.Add(links);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(links);
        }

        // GET: ShortUrlControllerSample/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null || _context.Links == null)
            {
                return NotFound();
            }

            var links = await _context.Links.FindAsync(id);
            if (links == null)
            {
                return NotFound();
            }
            return View(links);
        }

        // POST: ShortUrlControllerSample/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Url")] Links links)
        {
            if (id != links.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(links);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LinksExists(links.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(links);
        }

        // GET: ShortUrlControllerSample/Delete/5
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null || _context.Links == null)
            {
                return NotFound();
            }

            var links = await _context.Links
                .FirstOrDefaultAsync(m => m.Id == id);
            if (links == null)
            {
                return NotFound();
            }

            return View(links);
        }

        // POST: ShortUrlControllerSample/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id) 
        {
            if (_context.Links == null)
            {
                return Problem("Entity set 'MvcLinksContext.Links'  is null.");
            }
            var links = await _context.Links.FindAsync(id);
            if (links != null)
            {
                _context.Links.Remove(links);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LinksExists(string id)
        {
          return (_context.Links?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
