using System.ComponentModel.DataAnnotations;

namespace shorturl_cs.Models
{
    public class Links
    {
        public string Id { get; set; }
        public string Url { get; set; }
    }
}